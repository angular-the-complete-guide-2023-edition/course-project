import { Component, OnInit } from '@angular/core';
import { Ingredient } from 'src/app/shared/ingredient.model';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
})
export class ShoppingListComponent implements OnInit {
  ingredients: Ingredient[] = [
      new Ingredient('Cinamon', 100)
    , new Ingredient('Apple', 5)
    , new Ingredient('Tomatoes', 5)
  ];

  constructor () {}

  ngOnInit() {}

}
