import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Recipe} from '../../recipe.model' ;

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
})
export class RecipeItemComponent implements OnInit {
  @Input() recipe:Recipe;
  @Output() recipeClick: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {}

  onRecipeClick() {
    this.recipeClick.emit();
  }
}
