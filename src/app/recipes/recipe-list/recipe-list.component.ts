import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Recipe} from '../recipe.model' ;

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
})
export class RecipeListComponent implements OnInit {
  @Output() recipeClick: EventEmitter<Recipe> = new EventEmitter<Recipe>();

  recipes: Recipe[] = [
      new Recipe('Flais', 'Flais recipe', 'https://www.oleico.com/wp-content/uploads/2020/05/original-7754e065acbcd16585cf6e3715fade0e.jpeg')
    , new Recipe('Agrome', 'Agrome recipe for dummies', 'https://laopinion.com/wp-content/uploads/sites/3/2020/01/canva-pasta-tuna-salad-with-tomatoes-wild-rocket-black-olives-and-red-onion.jpg?quality=80&strip=all')
  ];

  constructor() { }

  ngOnInit(): void {
  }

  onRecipeSelected(recipe:Recipe) {
    this.recipeClick.emit(recipe);
  }
}
